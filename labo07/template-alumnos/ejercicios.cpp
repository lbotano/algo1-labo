#include "ejercicios.h"

vector<vector<int> > productoVectorial(vector<int> u, vector<int> v){
	vector<vector<int>> res(u.size(), vector<int>(v.size()));
    for (int i = 0; i < u.size(); i++) {
        for (int j = 0; j < v.size(); j++) {
            res[i][j] = u[i] * v[j];
        }
    }
	return res;
}

void swap(int& a, int& b) {
    int c = a;
    a = b;
    b = c;
}
void trasponer(vector<vector<int>> &m) {
    for (int i = 0; i < m.size(); i++) {
        for (int j = i + 1; j < m.size(); j++) {
            swap(m[i][j], m[j][i]);
        }
    }
	return;
}

int multiplicacionEnCelda(vector<vector<int>> m1, vector<vector<int>> m2, int i, int j) {
    int accu = 0;
    for (int k = 0; k < m1[0].size(); k++) {
        accu += m1[i][k] * m2[k][j];
    }
    return accu;
}
vector<vector<int>> multiplicar(vector<vector<int>> m1, vector<vector<int> > m2){
	vector<vector<int>> res(m1.size(), vector<int>(m2[0].size()));
    for (int i = 0; i < res.size(); i++) {
        for (int j = 0; j < res[0].size(); j++) {
            res[i][j] = multiplicacionEnCelda(m1, m2, i, j);
        }
    }
	return res;
}

int promedioVecinos(vector<vector<int>> m, int y, int x) {
    int suma = 0;
    int cant = 0;
    for (int i = y - 1; i <= y + 1; i++) {
        for (int j = x - 1; j <= x + 1; j++) {
            if (i >= 0 && i < m.size() && j >= 0 && j < m[0].size()) {
                suma += m[i][j];
                cant++;
            }
        }
    }
    return suma / cant;
}
vector<vector<int>> promediar(vector<vector<int>> m){
	vector<vector<int>> res(m.size(), vector<int>(m[0].size()));
    for (int i = 0; i < res.size(); i++) {
        for (int j = 0; j < res[0].size(); j++) {
            res[i][j] = promedioVecinos(m, j, i);
        }
    }
	return res;
}

bool esPico(vector<vector<int>> m, int x, int y) {
    for (int i = y - 1; i <= y + 1; i++) {
        for (int j = x - 1; j <= x + 1; j++) {
            if (
                i >= 0 && i < m.size() && j >= 0 && j < m[0].size()
                && !(i == y && j == x)
                && m[i][j] >= m[y][x]
            ) {
                return false;
            }
        }
    }
    return true;
}
int contarPicos(vector<vector<int>> m){
    int res = 0;
    for (int i = 0; i < m.size(); i++) {
        for (int j = 0; j < m[0].size(); j++) {
            if (esPico(m, j, i)) res++;
        }
    }
	return res;
}

bool esTriangularSuperior(vector<vector<int>> m) {
    if (m.size() < 2) return true;
    for (int i = 1; i < m.size(); i++) {
        for (int j = 0; j < i; j++) {
            if (m[i][j] != 0) {
                return false;
            }
        }
    }
    return true;
}
bool esTriangularInferior(vector<vector<int>> m) {
    if (m.size() < 2) return true;
    for (int i = 0; i < m.size(); i++) {
        for (int j = i + 1; j < m.size(); j++) {
            if (m[i][j] != 0) {
                return false;
            }
        }
    }
    return true;
}
bool esTriangular(vector<vector<int> > m){
	return esTriangularInferior(m) || esTriangularSuperior(m);
}

bool tieneAmenaza(vector<vector<int>> m, int x, int y) {
    // Vertical
    for (int i = 0; i < m.size(); i++) {
        if (i != y && m[i][x]) return true;
    }

    // Horizontal
    for (int j = 0; j < m[0].size(); j++) {
        if (j != x && m[y][j]) return true;
    }

    // Diagonal Descendiente
    for (int i = 1; y + i < m.size() && x + i < m[0].size(); i++) {
        if (m[y + i][x + i] == 1) return true;
    }
    for (int i = 1; y - i >= 0 && x - i >= 0; i++) {
        if (m[y - i][x - i] == 1) return true;
    }

    // Diagonal Ascendiente
    for (int i = 1; y - i >= 0 && x + i < m[0].size(); i++) {
        if (m[y - i][x + i] == 1) return true;
    }
    for (int i = 1; y + i < m.size() && x - i >= 0; i++) {
        if (m[y + i][x - i] == 1) return true;
    }

    return false;
}
bool hayAmenaza(vector<vector<int> > m){
    for (int i = 0; i < m.size(); i++) {
        for (int j = 0; j < m[0].size(); j++) {
            if (m[i][j] == 1 && tieneAmenaza(m, j, i)) return true;
        }
    }
	return false;
}

int diferenciaDiagonales(vector<vector<int>> m) {
    int sumDiagonalDesc = 0;
    int sumDiagonalAsc = 0;
    for (int i = 0; i < m.size(); i++) {
        sumDiagonalDesc += m[i][i];
    }
    for (int i = 0; i < m.size(); i++) {
        sumDiagonalAsc += m[m.size() - 1 - i][i];
    }
    return abs(sumDiagonalAsc - sumDiagonalDesc);
}