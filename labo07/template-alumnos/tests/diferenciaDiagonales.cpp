#include "../ejercicios.h"
#include "gtest/gtest.h"

TEST(DiferenciaDiagonales, Test1) {
    vector<vector<int> > m = {
            {10,2,3,4},
            {5,10,7,8},
            {10,9,8,7},
            {4,0,0,1}
    };
    EXPECT_EQ(diferenciaDiagonales(m), 5);
}

TEST(DiferenciaDiagonales, Test2) {
    vector<vector<int> > m = {
            {1,2,3,4},
            {5,6,7,8},
            {10,9,8,7},
            {0,0,0,1}
    };
    EXPECT_EQ(diferenciaDiagonales(m), 4);
}

TEST(DiferenciaDiagonales, Test3) {
    vector<vector<int> > m = {
            {1,2,3,1},
            {5,2,2,8},
            {10,3,3,7},
            {4,0,0,4}
    };
    EXPECT_EQ(diferenciaDiagonales(m), 0);
}

TEST(DiferenciaDiagonales, Test4) {
    vector<vector<int> > m = {
            {-1,2,3,-4},
            {5,-6,7,8},
            {1,-9,-8,7},
            {0,0,0,1}
    };
    EXPECT_EQ(diferenciaDiagonales(m), 8);
}

TEST(DiferenciaDiagonales, Test5) {
    vector<vector<int> > m = {
            {-1,2},
            {6,-6}
    };
    EXPECT_EQ(diferenciaDiagonales(m), 15);
}

TEST(DiferenciaDiagonales, Test6) {
    vector<vector<int> > m = {
            {-1}
    };
    EXPECT_EQ(diferenciaDiagonales(m), 0);
}
