#include "../ejercicios.h"
#include "gtest/gtest.h"

TEST(EsTriangular, Test1) {
    vector<vector<int> > m = {
            {1,2,0,4},
            {0,2,0,0},
            {0,0,3,4},
            {0,0,0,4}
    };
    EXPECT_TRUE(esTriangular(m));
}

TEST(EsTriangular, Test2) {
    vector<vector<int> > m = {
            {1,0,0,0},
            {0,2,0,0},
            {0,8,3,0},
            {0,0,0,4}
    };
    EXPECT_TRUE(esTriangular(m));
}

TEST(EsTriangular, Test3) {
    vector<vector<int> > m = {
            {1,0,0,0},
            {0,2,3,0},
            {0,0,3,0},
            {1,0,0,0}
    };
    EXPECT_FALSE(esTriangular(m));
}

TEST(EsTriangular, Test4) {
    vector<vector<int> > m = {
            {0,0,0,4},
            {0,0,0,0},
            {0,0,0,0},
            {1,0,0,0}
    };
    EXPECT_FALSE(esTriangular(m));
}

TEST(EsTriangular, Test5) {
    vector<vector<int> > m = {
            {1,8},
            {0,2},
    };
    EXPECT_TRUE(esTriangular(m));
}
