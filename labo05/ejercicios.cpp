#include "ejercicios.h"


// Ejercicio 1
// No hace falta cambiar el código cuando cambia el invariante porque las dos expresiones son equivalentes
bool existePico(vector<int> v){
    bool res = false;
    for(int i = 1; i < v.size() - 1; i++){
        if(v[i] > v[i-1] && v[i] > v[i+1])
            res = true;
    }
	return res;
}

// Ejercicio 2
int maximo(int a, int b){
    if(a>b)
        return a;
    else
        return b;
}

int minimo(int a, int b){
    if(a<b)
        return a;
    else
        return b;
}

int mcd(int m, int n){
    int a = maximo(m, n);
    int b = minimo(m, n);
    while (b>0) {
        int resto = a%b;
        a = b;
        b = resto;
    }
	return a;
}

// Ejercicio 3
int indiceMinSubsec(vector<int> v, int l, int r){
    int res = l;
    for(int i = r; i >= l; i--){
        if(v[i]<v[res])
            res = i;
    }
	return res;
}

// Ejercicio 4

void swap (int& a, int& b) {
    int tmp = a;
    a = b;
    b = tmp;
}
void ordenar1(vector<int>& v){
    for(int i = 0; i < v.size(); i++){
        int posMenorDelante = indiceMinSubsec(v, i, v.size()-1);
        swap(v[i], v[posMenorDelante]);
    }
	return;
}

// Ejercicio 5
/*
 * +---+---+---+---+
 * | 0 | 1 |   | 2 |
 * +---+---+---+---+
 * 0   i   j   k  |s|
 */

void ordenar2(vector<int>& v){
    int i = 0;
    int j = 0;
    int k = v.size() - 1;

    while(j<=k){
        if(v[j]==0) {
            swap(v[j], v[i]);
            i++;
            j++;
        }else if(v[j]==1){
            j++;
        }else if(v[j]==2){
            swap(v[j], v[k]);
            k--;
        }
    }
	return;
}

// Ejercicio 6
tuple<int,int> division(int n, int d){
    int r = n;
    int q = 0;
    while (r >= d) {
        q++;
        r-=d;
    }
	return make_tuple(q,r);
}
