#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "anagramas.h"
using namespace std;

int main(){
    string palabra, palabra2;
    cout << "Ingrese una palabra en ingles con letras en minuscula:" << endl;
    cin >> palabra;

    ifstream dicci ("ingles.dic");
    vector<string> listaPalabras;
    string word;
    while(getline(dicci, word)){
        word.pop_back();
        listaPalabras.push_back(word);
    }
    int cantidadAnagrama = 0;
    for (int i = 0; i < listaPalabras.size(); ++i)
    {
        if (esAnagrama(listaPalabras[i], palabra)) {
            cout << "Son anagramas: " << listaPalabras[i] << " y " << palabra << endl;
            cantidadAnagrama++;
        }
    }
    cout << "La cantidad de anagramas es: " << cantidadAnagrama << endl;
    return 0;
}

bool esAnagrama(string p1, string p2){
    // CAMBIAR AQUI QUE IMPLEMENTACION USAR
    return esAnagrama1(p1,p2);
}

bool esAnagrama1(string p1, string p2){
    //devuelve true si p1 es anagrama de p2
    //esta versión usa ordenar
    ordenarString(p1);
    ordenarString(p2);
    return p1 == p2;
}

bool esAnagrama2(string p1, string p2){
    //COMPLETAR
    //devuelve true si p1 es anagrama de p2
    //esta versión usa el mapeo de letras a números primos. Utilizar charToPrimo()
    int val1 = 1;
    for (int i = 0; i < p1.size(); i++) {
        val1 *= charToPrimo(p1[i]);
    }

    int val2 = 1;
    for (int i = 0; i < p2.size(); i++) {
        val2 *= charToPrimo(p2[i]);
    }
	return val1 == val2;
}

void ordenarString(string &palabra){
    //COMPLETAR
    //Asumir que palabra solo contiene letras minusculas de a-z (sin enie). Implementar cualquier algoritmo
    for (int i = 0; i < palabra.size() - 1; i++) {
        int maximo = i;
        for (int j = i + 1; j < palabra.size(); j++) {
            if (palabra[j] > palabra[maximo]) {
                maximo = j;
            }
        }
        swap(palabra[i], palabra[maximo]);
    }
}

int charToPrimo(char c){
    //COMPLETAR
    //Dado una letra minuscula de a-z (sin enie) devolver el numero primo correspondiente.
    const int primos[] = {
            2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101
    };
    return primos[posicionEnAlfabeto(c)];
	return 0;
}

int posicionEnAlfabeto(char c){
    return c - 'a';
}
