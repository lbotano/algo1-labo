#include <iostream>

// 2) y 3)
int f(int x, int y) {
  if (x > y) {
    return x + y;
  }
  return x * y;
}

// 4)
bool esPrimo(int n) {
  for (int i = 2; i < n; i++) {
    if (n % i == 0) {
      return false;
    }
  }
  return true;
}

// 5)
int fibonacci(int n) {
  if (n == 0) {
    return 0;
  } else if (n == 1) {
    return 1;
  }
  return fibonacci(n - 1) + fibonacci(n - 2);
}

int fibonacciLoop(int n) {
  int x = 0, y = 1;
  for (int i = 1; i < n; i++) {
    int swap = x;
    x = y;
    y = swap + x;
  }
  if (n > 0) {
    return y;
  }
  return 0;
}

// 6)
int sumaImpares(int n) {
  if (n <= 1) {
    return 0;
  }
  if (n % 2 == 0) {
    return n - 1 + sumaImpares(n - 1);
  }
  return sumaImpares(n - 1);
}

int sumaImparesLoop(int n) {
  int result = 0;
  for (int i = 1; i < n; i += 2) {
    result += i;
  }
  return result;
}

// 7)
int divisoresHasta(int n, int hasta) {
  if (n == 1) {
    return 1;
  }
  if (hasta % n == 0) {
    return n + divisoresHasta(n - 1, hasta);
  }
  return divisoresHasta(n - 1, hasta);
}

int sumaDivisores(int n) {
  return divisoresHasta(n, n);
}

int sumaDivisoresLoop(int n) {
  int accu = 1;
  for (int i = 2; i <= n; i++) {
    if (n % i == 0) {
      accu += i;
    }
  }
  return accu;
}

// 8)
int combinatorio(int n, int k) {
  if (k == 0 || k == n) {
    return 1;
  }
  return combinatorio(n - 1, k - 1) + combinatorio(n - 1, k);
}

int main () {
  std :: cout << "El resultado es: " << combinatorio(3, 1) << std :: endl;
  return 0;
}
