#include <iostream>
#include <cmath>

int potencia(int a, int b) {
  int result = 1;
  for (int i = 0; i < b; i++) {
    result *= a;
  }
  return result;
}

int main() {
  std::cout << "Ingrese un número en binario: ";
  int binario;
  std::cin >> binario;

  // Averiguar cantidad de dígitos
  int digitos = 1;
  while (binario / potencia(10, digitos) != 0) {
    digitos++;
  }

  // Calcular resultado
  int result = 0;
  for (int i = 0; i < digitos; i++) {
    int digito = ((binario % potencia(10, digitos - i)) / potencia(10, digitos - i - 1));
    result += digito * potencia(2, digitos - i - 1);
  }

  std::cout << "En decimal es: " << result << std::endl;
}
