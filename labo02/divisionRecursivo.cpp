#include <iostream>

int division(int dividendo, int divisor, int& resto) {
  if (dividendo < divisor) {
    resto = dividendo;
    return 0;
  } else {
    return 1 + division(dividendo - divisor, divisor, resto);
  }
}

int main() {
  std::cout << "Dividendo: ";
  int dividendo;
  std::cin >> dividendo;

  std::cout << "Divisor: ";
  int divisor;
  std::cin >> divisor;

  int resto;
  int cociente = division(dividendo, divisor, resto);

  std::cout << dividendo << " = " << cociente << " * " << divisor << " + " << resto << std::endl;
}
