#include <iostream>

void collatz(int n, int& cantPasos) {
  cantPasos = 1;
  while (n != 1) {
    if (n % 2 == 0) {
      n /= 2;
    } else {
      n = 3*n + 1;
    }
    cantPasos++;
  }
}

int main() {
  int cantPasos;
  int numero;
  std::cin >> numero;
  collatz(numero, cantPasos);
  std::cout << cantPasos << std::endl;
}
