#include <iostream>

int division(int dividendo, int divisor, int& resto) {
  int cociente = 0;
  while (dividendo >= divisor) {
    dividendo -= divisor;
    cociente++;
  }
  resto = dividendo;
  return cociente;
}

int main() {
  std::cout << "Dividendo: ";
  int dividendo;
  std::cin >> dividendo;

  std::cout << "Divisor: ";
  int divisor;
  std::cin >> divisor;

  int resto;
  int cociente = division(dividendo, divisor, resto);

  std::cout << dividendo << " = " << cociente << " * " << divisor << " + " << resto << std::endl;
}
