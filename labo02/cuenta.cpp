#include <iostream>

int triplicarPorValor(int cuenta) {
  return cuenta * 3;
}

void triplicarPorReferencia(int& cuenta) {
  cuenta *= 3;
}

int main() {
  int cuenta = 100;
  triplicarPorValor(cuenta); // No modifica el valor de cuenta
  triplicarPorReferencia(cuenta); // Sí modifica el valor de cuenta
  std::cout << cuenta << std::endl;
}
