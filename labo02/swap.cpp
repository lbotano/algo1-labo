#include <iostream>

void swap(int& a, int&b) {
  b += a;
  a = b - a;
  b -= a;
}

int main() {
  int a = 10;
  int b = 23;
  swap(a, b);
  std::cout << a << " " << b << std::endl;
}
