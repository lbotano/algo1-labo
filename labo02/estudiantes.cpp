#include <iostream>

int main() {
  int suma = 0;
  int cantNotas = 0;
  int nota;
  int aprobados = 0;
  while (nota != -1) {
    std::cout << "Ingrese la nota " << cantNotas << " (-1 para terminar): ";
    std::cin >> nota;
    if (nota >= 6)
      aprobados++;
    if (nota != -1) {
      suma += nota;
      cantNotas++;
    }
  }
  if (cantNotas < 6) {
    std::cout << "Tiene que ingresar al menos 6 notas" << std::endl;
  } else {
    float promedio = (float) suma / cantNotas;
    std::cout << "Suma: " << suma << std::endl;
    std::cout << "Promedio: " << promedio << std::endl;
    std::cout << "Aprobados: " << aprobados << std::endl;
    std::cout << "Reprobados: " << cantNotas - aprobados << std::endl;
  }
}
