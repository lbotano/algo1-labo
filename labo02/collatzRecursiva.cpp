#include <iostream>

void collatz(int n, int& cantPasos) {
  cantPasos++;
  if (n != 1) {
    if (n % 2 == 0) {
      collatz(n / 2, cantPasos);
    } else {
      collatz(3*n + 1, cantPasos);
    }
  }
}

int main() {
  int pasos = 0;
  int n;
  std::cin >> n;
  collatz(n, pasos);
  std::cout << pasos << std::endl;
}
