#include <iostream>

#define PI 3.14159

void datosCirculo(int radio) {
  std::cout << "Diámetro: " << radio * 2 << std::endl;
  std::cout << "Circunferencia: " << radio * 2 * PI << std::endl;
  std::cout << "Área: " << PI * radio * radio << std::endl;
}

int main() {
  std::cout << "Ingrese radio del círculo: ";
  float radio;
  std::cin >> radio;
  datosCirculo(radio);
}
