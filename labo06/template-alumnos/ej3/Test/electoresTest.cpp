#include "../electores.h"
#include "gtest/gtest.h"

TEST(EsPosterior, EsPosterior) {
    bool res = esPosteriorA(2022, 10, 5, 1958, 5, 6);
    EXPECT_TRUE(res);
}

TEST(EsPosterior, SonIguales) {
    bool res = esPosteriorA(2019, 1, 2, 2019, 1, 2);
    EXPECT_FALSE(res);
}

TEST(EsPosterior, NoEsPosterior) {
    bool res = esPosteriorA(1958, 5, 6, 2022, 10, 5);
    EXPECT_FALSE(res);
}

TEST(Electores, Dia29NoBisiesto) {
    int res = validarVotante(2021, 2, 29);
    EXPECT_EQ(res, 4);
}

TEST(Electores, Dia29Bisiesto) {
    int res = validarVotante(2020, 2, 29);
    EXPECT_EQ(res, 0);
}

TEST(Electores, AntesDeCristo) {
    int res = validarVotante(-100, 2, 29);
    EXPECT_EQ(res, 4);
}

TEST(Electores, DespuesDeElecciones) {
    int res = validarVotante(2022, 10, 29);
    EXPECT_EQ(res, 4);
}

TEST(Electores, DiaNegativo) {
    int res = validarVotante(2020, 2, -10);
    EXPECT_EQ(res, 4);
}

TEST(Electores, DiaMuyGrande) {
    int res = validarVotante(2020, 2, 32);
    EXPECT_EQ(res, 4);
}

TEST(Electores, MesNegativo) {
    int res = validarVotante(2020, -2, 1);
    EXPECT_EQ(res, 4);
}

TEST(Electores, MesMuyGrande) {
    int res = validarVotante(2020, 13, 1);
    EXPECT_EQ(res, 4);
}

TEST(Electores, Dia31Correcto) {
    int res = validarVotante(2020, 12, 31);
    EXPECT_EQ(res, 0);
}

TEST(Electores, Dia31Incorrecto) {
    int res = validarVotante(2020, 11, 31);
    EXPECT_EQ(res, 4);
}

TEST(Electores, OpcionalMenor) {
    int res = validarVotante(2006, 6, 5);
    EXPECT_EQ(res, 1);
}

TEST(Electores, OpcionalMenorMismoCumple) {
    int res = validarVotante(2006, 10, 22);
    EXPECT_EQ(res, 1);
}

TEST(Electores, Obligatorio) {
    int res = validarVotante(2004, 6, 5);
    EXPECT_EQ(res, 2);
}

TEST(Electores, ObligatorioMismoCumple) {
    int res = validarVotante(2004, 10, 22);
    EXPECT_EQ(res, 2);
}

TEST(Electores, OpcionalMayor) {
    int res = validarVotante(1952, 6, 5);
    EXPECT_EQ(res, 3);
}

TEST(Electores, OpcionalMayorMismoCumple) {
    int res = validarVotante(1952, 10, 22);
    EXPECT_EQ(res, 3);
}