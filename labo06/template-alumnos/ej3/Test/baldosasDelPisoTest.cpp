#include "../baldosasDelPiso.h"
#include "gtest/gtest.h"

TEST(BaldosasCajaNegra, MNDivisibles) {
    int res = baldosasDelPiso(100, 100, 10);
    EXPECT_EQ(res, 100);
}

TEST(BaldosasCajaNegra, MDivisible) {
    int res = baldosasDelPiso(100, 95, 10);
    EXPECT_EQ(res, 100);
}

TEST(BaldosasCajaNegra, NDivisible) {
    int res = baldosasDelPiso(94, 100, 10);
    EXPECT_EQ(res, 100);
}

TEST(BaldosasCajaNegra, NingunoDivisible) {
    int res = baldosasDelPiso(95, 96, 10);
    EXPECT_EQ(res, 100);
}

// Al hacer los casos con caja blanca me quedarian los mismos casos que en las particiones propuestas para la caja negra