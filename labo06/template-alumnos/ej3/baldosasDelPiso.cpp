int baldosasDelPiso(int M, int N, int B) {
    if (M % B != 0) M = M/B + 1;
    else M = M/B;

    if (N % B != 0) N = N/B + 1;
    else N = N/B;

    return M * N;
}
