
#define NO_VOTA             0
#define OPCIONAL_MENOR      1
#define OBLIGATORIO         2
#define OPCIONAL_MAYOR      3
#define ERROR               4

bool esPosteriorA(int a1, int m1, int d1, int a2, int m2, int d2);
bool esBisiesto(int a);
bool mesesYDiasSonValidos(int a, int m, int d);
int validarVotante(int a, int m, int d);
