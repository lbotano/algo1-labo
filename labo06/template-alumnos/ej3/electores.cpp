#include "electores.h"
#define AÑO 2022

bool esPosteriorA(int a1, int m1, int d1, int a2, int m2, int d2) { // No incluye fecha igual
    return (a1 > a2) ||
           (a1 == a2 && m1 > m2) ||
           (a1 == a2 && m1 == m2 && d1 > d2);
}

bool esBisiesto(int a) {
    return a % 4 == 0 && a % 100 != 0;
}

bool mesesYDiasSonValidos(int a, int m, int d) {
    if (m > 12 || m < 1) return false;
    if (d < 1) return false;
    if (d < 1 || d > 31) return false;
    if (d == 31) return m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12;
    if (m == 2 && d > 29) return false;
    if (m == 2 && d == 29) return esBisiesto(a);
    return true;
}

// Se vota el 22 de octubre de 2022
int validarVotante(int a, int m, int d) {
    if (!mesesYDiasSonValidos(a, m, d)) {
        return 4;
    }

    if (esPosteriorA(a, m, d, AÑO - 16, 10, 22)
    && !esPosteriorA(a, m, d, AÑO, 10, 22)
    ) {
        return 0; // NO_VOTA
    }
    if (esPosteriorA(a, m, d, AÑO - 18, 10, 22)
    && !esPosteriorA(a, m, d, AÑO - 16, 10, 22)
    ) {
        return 1; // OPCIONAL_MENOR
    }
    if (esPosteriorA(a, m, d, AÑO - 70, 10, 22)
    && !esPosteriorA(a, m, d, AÑO - 18, 10, 22)
    ) {
        return 2; // OBLIGATORIO
    }
    if (esPosteriorA( a, m, d, 1, 0, 0)
    && !esPosteriorA(a, m, d, AÑO - 70, 10, 22)
    ) {
        return 3; // OPCIONAL_MAYOR
    }

    return 4; // ERROR
}
