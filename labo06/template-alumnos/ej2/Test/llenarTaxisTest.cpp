#include "../llenarTaxis.h"
#include "gtest/gtest.h"

// Escribir tests aca:

//
// llenarTaxis1
//

TEST(LlenarTaxis1, TodasMismoValor) {
    int result = llenarTaxis1(3, 3, 3);
    EXPECT_EQ(result, 5);
}

TEST(LlenarTaxis1, DiferentesG2Par) {
    int result = llenarTaxis1(3, 2, 1);
    EXPECT_EQ(result, 2);
}

TEST(LlenarTaxis1, DiferentesG2Impar1A) { // g1 > 0, g3 > 0, |g1 - g3| = 1
    int result = llenarTaxis1(1, 3, 2);
    EXPECT_EQ(result, 4);
}

TEST(LlenarTaxis1, DiferentesG2Impar1B) { // g1 > 0, g3 > 0, |g1 - g3| = 2
    int result = llenarTaxis1(2, 3, 4);
    EXPECT_EQ(result, 6);
}
TEST(LlenarTaxis1, DiferentesG2Impar2) { // g1 > 0, g3 > 0, |g1 - g3| ≠ 1 o 2
    int result = llenarTaxis1(2, 3, 4);
    EXPECT_EQ(result, 6);
}

//
// llenarTaxis2
//

TEST(LlenarTaxis2, TodasMismoValor) {
    int result = llenarTaxis2(3, 3, 3);
    EXPECT_EQ(result, 5);
}

TEST(LlenarTaxis2, DiferentesG2Par) {
    int result = llenarTaxis2(3, 2, 1);
    EXPECT_EQ(result, 2);
}

TEST(LlenarTaxis2, DiferentesG2Impar1A) { // g1 > 0, g3 > 0, |g1 - g3| = 1
    int result = llenarTaxis2(1, 3, 2);
    EXPECT_EQ(result, 4);
}

TEST(LlenarTaxis2, DiferentesG2Impar1B) { // g1 > 0, g3 > 0, |g1 - g3| = 2
    int result = llenarTaxis2(2, 3, 4);
    EXPECT_EQ(result, 6);
}
TEST(LlenarTaxis2, DiferentesG2Impar2) { // g1 > 0, g3 > 0, |g1 - g3| ≠ 1 o 2
    int result = llenarTaxis2(2, 3, 4);
    EXPECT_EQ(result, 6);
}

//
// llenarTaxis3
//

TEST(LlenarTaxis3, TodasMismoValor) {
    int result = llenarTaxis3(3, 3, 3);
    EXPECT_EQ(result, 5);
}

TEST(LlenarTaxis3, DiferentesG2Par) {
    int result = llenarTaxis3(3, 2, 1);
    EXPECT_EQ(result, 2);
}

TEST(LlenarTaxis3, DiferentesG2Impar1A) { // g1 > 0, g3 > 0, |g1 - g3| = 1
    int result = llenarTaxis3(1, 3, 2);
    EXPECT_EQ(result, 4);
}

TEST(LlenarTaxis3, DiferentesG2Impar1B) { // g1 > 0, g3 > 0, |g1 - g3| = 2
    int result = llenarTaxis3(2, 3, 4);
    EXPECT_EQ(result, 6);
}
TEST(LlenarTaxis3, DiferentesG2Impar2) { // g1 > 0, g3 > 0, |g1 - g3| ≠ 1 o 2
    int result = llenarTaxis3(2, 3, 4);
    EXPECT_EQ(result, 6);
}