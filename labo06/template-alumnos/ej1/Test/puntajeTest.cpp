#include "../puntaje.h"
#include "gtest/gtest.h"

// Escribir tests aca:
TEST(PuntajeTest, MenorNoMultiplo) {
    int b = 4;
    int result = puntaje(b);
    EXPECT_EQ(result, -2);
}

TEST(PuntajeTest, MenorMultiplo) {
    int b = 6;
    int result = puntaje(b);
    EXPECT_EQ(result, 22);
}

TEST(PuntajeTest, MayorNoMultiplo) {
    int b = 11;
    int result = puntaje(b);
    EXPECT_EQ(result, 1);
}

TEST(PuntajeTest, MayorMultiplo) {
    int b = 12;
    int result = puntaje(b);
    EXPECT_EQ(result, 22);
}