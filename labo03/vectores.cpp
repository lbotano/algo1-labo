#include "vectores.h"

// Función para probar en main.cpp si este módulo fue cargado correctamente
void holaModuloVectores(){
    cout << "El módulo vectores fue cargado correctamente" << endl;
}


//Ejercicio
bool divide(vector<int> v, int a){
	// Dados un vector v y un int a, decide si a divide a todos los numeros de v.
    for (int i = 0; i < v.size(); i++) {
        if (v[i] == 0 || a % v[i] != 0) return false;
    }
    return true;
}

//Ejercicio
int mayor(vector<int> v){
	// Dado un vector v, devuelve el valor maximo encontrado.
    int max = v[0];
    for (int i = 1; i < v.size(); i++) {
        if (v[i] > max) max = v[i];
    }
    return max;
}

//Ejercicio
vector<int> reverso(vector<int> v){
	// Dado un vector v, devuelve el reverso.
    vector<int> reverso(v.size());
    for (int i = 0; i < reverso.size(); i++) {
        reverso[i] = v[v.size() - 1 - i];
    }
    return reverso;
}

//Ejercicio
vector<int> rotar(vector<int> v, int k){
    // Dado un vector v y un entero k, rotar k posiciones los elementos de v.
    // [1,2,3,4,5,6] rotado 2, deberia dar [3,4,5,6,1,2].
    vector<int> rotado(v.size());
    for (int i = 0; i < rotado.size(); i++) {
        rotado[i] = v[(i - k) % v.size()];
    }
    return rotado;
}

//Ejercicio
bool estaOrdenado(vector<int> v){
  if (v.size() <= 1) return true;
  bool empiezaDecreciente = v[0] > v[1];
  for (int i = 0; i < v.size() - 1; i++) {
    if (empiezaDecreciente && v[i] < v[i + 1] || !empiezaDecreciente && v[i] > v[i+1])
      return false;
  }
  return true;
}

//Ejercicio
vector<int> factoresPrimos(int n){
	//que dado un entero devuelve un vector con los factores primos del mismo
    vector<int> result;
    for(int i = 2; i < n; i++) {
        bool iEsPrimo = true;
        for (int k = 2; k < i; k++) {
            if (i % k == 0) iEsPrimo = false;
        }
        if (iEsPrimo && n % i == 0) result.push_back(i);
    }
    return result;
}

//Ejercicio
void mostrarVector(vector<int> v){
	//que dado un vector de enteros muestra por la salida estándar, el vector
	// Ejemplo: si el vector es <1, 2, 5, 65> se debe mostrar en pantalla [1, 2, 5, 65]
    if (v.size() >= 0) std::cout << "[" << v[0];
    for (int i = 1; i < v.size(); i++) {
        std::cout << "," << v[i];
    }
    std::cout << "]" << std::endl;
}

//Ejercicio
vector<int> leerVector(string nombreArchivo) {
    vector<int> v;
    ifstream archivoIn;
    archivoIn.open("archivos/" + nombreArchivo, ios::in);
    while (!archivoIn.eof()) {
        int n;
        archivoIn >> n;
        v.push_back(n);
    }
    archivoIn.close();
    return v;
}

//Ejercicio
void guardarVector(vector<int> v, string nombreArchivo) {
    ofstream archivoOut;
    archivoOut.open("archivos/" + nombreArchivo);
    archivoOut << "[";
    if (v.size() > 0) archivoOut << v[0];
    for (int i = 1; i < v.size(); i++) {
        archivoOut << "," << v[i];
    }
    archivoOut << "]";
    archivoOut.close();
}

//Ejercicio
int elementoMedio(vector<int>v) {
    for (int i = 0; i < v.size(); i++) {
        int sumaIzquierda = 0;
        int sumaDerecha = 0;
        for (int k = 0; k < i + 1; k++) sumaIzquierda += v[k];
        for (int j = i + 1; j < v.size(); j++) {
            sumaDerecha += v[j];
        }
        if (sumaIzquierda > sumaDerecha) return v[i];
    }
}

int numeroApariciones(int n, vector<int> v) {
    int result = 0;
    for (int i = 0; i < v.size(); i++) {
        if (v[i] == n) result++;
    }
    return result;
}

vector<int> eliminarRepetidos(vector<int> v) {
    vector<int> result;
    for (int i = 0; i < v.size(); i++) {
        if (numeroApariciones(v[i], result) < 1) result.push_back(v[i]);
    }
    return result;
}

//Ejercicio
void cantApariciones(string nombreArchivo) {
    vector<int> v = leerVector(nombreArchivo);
    vector<int> sinRepetidos = eliminarRepetidos(v);
    vector<int> cantApariciones;
    ofstream archivoOut;
    archivoOut.open("archivos/out/" + nombreArchivo);
    for (int i = 0; i < sinRepetidos.size(); i++) {
        archivoOut << "linea " << i + 1 << ": " << sinRepetidos[i] << " " << numeroApariciones(sinRepetidos[i], v) << std::endl;
    }
    archivoOut.close();
}
