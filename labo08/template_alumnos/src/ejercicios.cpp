#include "ejercicios.h"
#include <iostream>

/* Para compilar */ 

using namespace std;

vector<string> split(string s, char delim) {
    vector<string> v;
    string palabra;
    for (int i = 0; i < s.size(); i++) {
        if (s[i] == delim) {
            if (palabra.size() > 0) v.push_back(palabra);
            palabra.clear();
        } else {
            palabra.push_back(s[i]);
        }
    }
    if (palabra.size() > 0) v.push_back(palabra);
	return v;
}

string darVueltaString(string s) {
    string res;
    for (int i = s.size() - 1; i >= 0; i--) {
        res.push_back(s[i]);
    }
    return res;
}

string vectorAString(vector<string> v) {
    string res;
    for (int i = 0; i < v.size(); i++) {
        for (int j = 0; j < v[i].size(); j++) {
            res.push_back(v[i][j]);
        }
        if (i < v.size() - 1) res.push_back(' ');
    }
    return res;
}

string darVueltaPalabra(string s){
    vector<string> resVector;
    string palabra;
    for (int i = s.size() - 1; i >= 0; i--) {
        if (s[i] != ' ') {
            palabra.push_back(s[i]);
        } else if (palabra.size() > 0){
            resVector.push_back(darVueltaString(palabra));
            palabra.clear();
        }
    }
    if (palabra.size() > 0) resVector.push_back(darVueltaString(palabra));

	return vectorAString(resVector);
}

// Decidir si s1 es substring o subsecuencia de s2
bool subsecuencia(string s1, string s2) {
    int cont = 0;
    for (int i = 0; i < s2.size(); i++) {
        if (s2[i] == s1[cont]) cont++;
    }
	return cont == s1.size();
}

int cantApariciones(char c, string s) {
    int res = 0;
    for (int i = 0; i < s.size(); i++) {
        if (s[i] == c) res++;
    }
    return res;
}

bool esAnagramaDe(string s1, string s2) {
    if (s1.size() != s2.size()) return false;
    for (int i = 0; i < s1.size(); i++) {
        if (cantApariciones(s1[i], s1) != cantApariciones(s1[i], s2))
            return false;
    }
    return true;
}

bool esAnagramaDeAlguno(string s, vector<string> ss) {
    return false;
}

// Mismo orden relativo de conjunto de anagramas y dentro de cada vector y con repetidos
vector<vector<string>> agruparAnagramas(vector<string> v) {
	vector<vector<string>> res;
    int grupoCont = 0;
    int stringsRestantes = v.size();

    while (stringsRestantes > 0) {
        for (int i = 0; i < v.size(); i++) {
            // Primero busca si la palabra ya está en algún grupo de anagramas
            bool esAnagramaNuevo = true;
            for (int j = 0; j < res.size(); j++) {
                esAnagramaNuevo &= res[j].size() == 0 || !esAnagramaDe(v[i], res[j][0]);
            }

            // Añade todos los anagramas al string encontrado
            if (esAnagramaNuevo) {
                res.push_back(vector<string>());
                res[grupoCont].push_back(v[i]);
                stringsRestantes--;
                for (int j = i + 1; j < v.size(); j++) {
                    if (esAnagramaDe(v[j], v[i])) {
                        res[grupoCont].push_back(v[j]);
                        stringsRestantes--;
                    }
                }
                grupoCont++;
            }
        }
    }
	return res;
}

bool esPalindromo(string s){
	return darVueltaString(s) == s;
}

bool tieneRepetidos(string s){
    bool distinto = true;
    for (int i = 0; i < (int)s.size() - 1; i++) {
        for (int j = i + 1; j < s.size(); j++) {
            if (s[i] == s[j]) distinto = false;
        }
    }
	return !distinto;
}

int restoPositivo(int a, int b) {
    return (a % b + b) % b;
}

string rotar(string s, int j){
	string res;
    for (int i = 0; i < s.size(); i++) {
        int pos = restoPositivo(i-j, s.size());
        res.push_back(s[pos]);
    }
	return res;
}

// no se puede usar substring
string darVueltaK(string s, int k) {
	string res;
    for (int i = 0; i < s.size() / k + 1; i++) {
        for (int j = k - 1; j >= 0; j--) {
            if (k*i + j < s.size()) res.push_back(s[k*i + j]);
        }
    }
	return res;
}

string abueloLaino(string s){
    string res;
    return res;
}

int cantidadDeFelicitaciones(vector<string> v){
    int res;
    return res;
}

int middleOut(string s, string t){

    return 0;
}
