#include <string>
#include <vector>
#include "risas.h"
#include <iostream>
#include <fstream>

using namespace std;

/* Utiles */
vector<char> leerDatos(string s) {
    vector<char> vec;
    return vec;
}

int leerLargoReal(string filename){
    int val = -1;
    return val;
}
/* Utiles */

/* RESOLUCION ALUMNOS */
int risaMasLarga(vector<char> s) {
    int largo = 0;
    bool ultimoFueH;
    for (int i = 0; i < s.size(); i++) {
        if (s[i] == 'h' || s[i] == 'a') {
            ultimoFueH = s[i] == 'h';

            int largoEncontrado = 1;
            bool sigueRiendo = true;
            int j = i + 1;
            while (sigueRiendo && j < s.size()) {
                if (s[j] == 'h' && !ultimoFueH || s[j] == 'a' && ultimoFueH) {
                    ultimoFueH = s[j] == 'h';
                    largoEncontrado++;
                } else {
                    sigueRiendo = false;
                }
                j++;
            }
            if (largoEncontrado > largo) largo = largoEncontrado;
        }
    }
    return largo;
}
/* RESOLUCION ALUMNOS */
