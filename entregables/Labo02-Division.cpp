#include <iostream>

int division(int dividendo, int divisor, int& resto){
    int cociente = 0;
    while (dividendo >= divisor) {
        dividendo -= divisor;
        cociente++;
    }
    resto = dividendo;
    return cociente;
}

using namespace std;
int main() {
    /* No hace falta modificar el main */
    //Leo la entrada
    int divisor,dividendo,cociente,resto;
    cin >> dividendo>>divisor;
    
    //Calculo la division
    cociente = division(dividendo,divisor,resto);
    
    //Salida
    cout << cociente << " " << resto;
    
    return 0;
}
