#include <iostream>
#include <string>

using namespace std;

int restoPositivo(int a, int b) {
    return (a % b + b) % b;
}

string rotar(string s, int j) {
    string res;
    for (int i = 0; i < s.size(); i++) {
        int pos = restoPositivo(i-j, s.size());
        res.push_back(s[pos]);
    }
    return res;
}

int main()
{
    /* No hace falta modificar el main */
    // Leo la entrada
    string s;
    int j;
    cin >> s >> j;
    
    string res = rotar(s, j);
    cout << res;
    return 0;
}
