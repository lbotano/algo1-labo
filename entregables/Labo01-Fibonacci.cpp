#include <iostream>

using namespace std;

// Funcion de fibonacci que calcula n-esimo termino
int fibonacci(int n){
    int x = 0, y = 1;
    for (int i = 1; i < n; i++) {
        int swap = x;
        x = y;
        y = swap + x;
    }
    if (n > 0) {
        return y;
    }
    return 0;
}
int main() 
{
    int n;
    cin >> n;
    cout << fibonacci(n) << endl;    
    return 0;
}
