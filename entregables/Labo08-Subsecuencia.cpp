#include <iostream>
#include <string>

using namespace std;

bool subsecuencia(string s1, string s2) {
    int cont = 0;
    for (int i = 0; i < s2.size(); i++) {
        if (s2[i] == s1[cont]) cont++;
    }
    return cont == s1.size();
}

int main()
{
    /* No hace falta modificar el main */
    // Leo la entrada
    string s, t;
    cin >> s >> t;
    
    bool res = subsecuencia(s, t);
    cout << (res ? "true" : "false");
    return 0;
}

